$(document).on('click', '.cerrarCarrera', function(){
   spinner.fadeIn('fast');
   terminarCarrera();
   socket.emit('log', 'El móvil '+storage.getItem('numero_taxi')+' ha terminado la carrera');
});

socket.on('verificar_carrera', function(data){

   var id_carrera =  data;
   var mi_id      =  storage.getItem('id_carrera');

   if ( id_carrera == mi_id )
   {
      //terminarCarrera();
      mostrarPagina('dash');
   }

});

function terminarCarrera()
{
   var id_carrera = storage.getItem('id_carrera');
   var url = api+'carrera/finalizar/'+id_carrera;

   $.post(url, function(){

   }).done(function(){
      limpiarStorageCarrera();
      mostrarPagina('dash');
      socket.emit('carrera_finalizada');

   }).fail(function(xhr, satus, err){
      nativeAlert('No se pudo cerrar la carrera'+id_carrera, 'solicite libre a operadora: '+err, 'Aceptar');
   });

   map.remove();
   spinner.fadeOut('fast');
}

function cargarCarrera()
{
   var nombre                 =  storage.getItem('nombre_cliente');
   var apellido               =  storage.getItem('apellido_cliente');
   var telefono               =  storage.getItem('telefono_cliente');
   var direccion              =  storage.getItem('direccion_cliente');
   var direccion_alternativa  =  storage.getItem('direccion_alternativa');
   var observacion            =  storage.getItem('observacion');

   nombre      =  nombre !== 'null' ? nombre : '--';
   apellido    =  apellido !== 'null' ? apellido : '--';
   observacion =  observacion !== 'null' ? observacion : '--';
   direccion   =  ( direccion_alternativa !== 'null' && direccion_alternativa !== "" ) ? direccion_alternativa : direccion;


   $('.desCliente.nombre').html(nombre+' '+apellido);
   $('.desCliente.telefono').html('<a href="tel:'+telefono+'">'+telefono+'</a>');
   $('.desCliente.direccion').html(direccion);
   $('.desCliente.observacion').html(observacion);

   initMap( direccion + ', Punta Arenas');

}

function carreraActiva()
{
   var mi_id   =  storage.getItem('id_taxi');
   var url     =  api+'carrera/buscar/'+mi_id;

   $.post(url, function(data){})

   .done(function(data){
      var info = data.info;
      if ( ! info )
      {
         clearInterval(checarCarrera);
         mostrarPagina('dash');
      } else {

      }
   })
   .fail(function(){

   });
}


function getGeolocation() {
   navigator.geolocation.getCurrentPosition( geolocationSuccess, geolocationSuccess, { maximuAge: 3000, timeout: 5000, enableHighAccuracy: true } );
}

var geolocationSuccess = function(position) {

   var data = {
      latitud     :  position.coords.latitude,
      longitud    :  position.coords.longitude,
      direccion   :  position.coords.heading,
      velocidad   :  position.coords.speed,
      numero_taxi :  storage.getItem('numero_taxi')
   };

   socket.emit( 'enviar_ubicacion', data );

}

function geolocationError(error) {

   alert('code: '    + error.code + '\n'+
         'mensaje: ' +error.message+'\n'
   );
}
