var mapCounter = 0;
var map;

function initMap( direccion ) {

   var div        =  document.getElementById('map');
   var address    =  direccion.replace(/^[0]+/g,"");
   map            =  plugin.google.maps.Map.getMap(div);

   plugin.google.maps.Geocoder.geocode( { 'address' : address }, function(results){

      if ( results.length ) {
         storage.setItem('hay_mapa', 'true');
         var result     =  results[0];
         var position   =  result.position;
         map.setBackgroundColor('#4e4f51');
         map.addMarker({
            'position'  :  position,
            'title'     :  JSON.stringify( result.position )
         }, function( marker ){

            map.moveCamera({
               'target' :  position,
               'zoom'   :  16
            }, function(){
               marker.shoeInfoWindow();
            });
         });
      }

   });

}
