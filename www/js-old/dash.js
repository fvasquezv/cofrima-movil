/**
* id = 1 para notificaciones de carrera
* TODO: verificar si hay carrera
**/

var timeout;
var aviso;
var mostrarBarra;

$(document).on('click', 'button.logOut', function(){

   var url  =  api+'cerrar-sesion';
   var id   =  storage.getItem('id_taxi');
   var data =  { id_taxi : id };
   $.post( url, data, function(){})

   .done(function(){
      socket.emit('log', 'El móvil '+storage.getItem('numero_taxi')+' ha cerrado sesión');
      cerrarSesion();
      ubicarSeccion();
   })
   .fail(function( xhr, status, err ){
      alert(err);
   });


});

$(document).on('click', '.btn-aceptar', function(){
   socket.emit('log', 'El móvil '+storage.getItem('numero_taxi')+' ha aceptado la carrera');
   aceptarCarrera();
});

$(document).on('click', '.buscarCarrera', function(){
   $('.barra-tiempo').css('width', '0%' );
   buscarCarrera();
   calcularPosicion();
});

$(document).on('click', '.btn-cancelar', function(){
   $('.barra-tiempo').css('width', '0%' );
   rechazarCarrera();
});

$(document).on('click', '.crearCarrera', function(){
   $('.barra-tiempo').css('width', '0%' );
   crearCarrera();
});

function cargarDatos()
{
   var nombre           =  storage.getItem('nombre') !== null ? storage.getItem('nombre') : '';
   var apellido         =  storage.getItem('apellido') !== null ? storage.getItem('apellido') : '';
   var nombre_completo  =  nombre+' '+apellido;

   $('.nombreConductor').html( nombre_completo );
   $('.numeroMovil').html( storage.numero_taxi+' ('+storage.patente_taxi+')'  );

   calcularPosicion();
}

function limpiarStorageCarrera()
{
   storage.removeItem( 'nombre_cliente');
   storage.removeItem( 'apellido_cliente');
   storage.removeItem( 'telefono_cliente');
   storage.removeItem( 'direccion_cliente');
   storage.removeItem( 'direccion_alternativa');
   storage.removeItem( 'id_carrera');
   storage.removeItem( 'en_carrera');
}

function crearCarrera()
{

   var url  =  api+'crear-carrera';
   var id   =  storage.getItem('id_taxi');
   var data =  { id_taxi : id };
   $.post( url, data, function(){})

   .done(function(){
      buscarCarrera();
   })
   .fail(function( xhr, status, err ){
      alert(err);
   });
}

function calcularPosicion()
{
   var url  =  api+'fichero/1/fichas';

   $('.posicionFichero').html('Calculando...');

   $.get( url, function(data){

   }).done(function(data){

      var disponibles   =  data.taxis.disponibles;
      var posicion      =  0;
      var encontrado    =  true;

      while ( encontrado )
      {
         var idTaxi    =  disponibles[posicion].id_taxi;
         var idActual   =  storage.id_taxi;


         if ( idTaxi == idActual )
         {
            encontrado = false;

         } else {
            posicion++;
            encontrado = true;
         }

      }

      var pos  =  posicion + 1;
      $('.posicionFichero').html(pos);

   }).fail(function(data){
      alert('no hay conexion con la API');
   });
}

function mostrarAviso()
{
   var direccion              =  storage.getItem('direccion_cliente');
   var direccion_alternativa  =  storage.getItem('direccion_alternativa');
   var telefono               =  storage.getItem('telefono_cliente');
   var id_carrera             =  storage.getItem('id_carrera');

   direccion   =  ( direccion_alternativa !== 'null' && direccion_alternativa !== "" ) ? direccion_alternativa : direccion;

   $('.alert-data.direccion').html(direccion);
   $('.alert-data.telefono').html(telefono);

   $('.btn-aceptar').attr('x-data-id', id_carrera);
   $('.btn-cancelar').attr('x-data-id', id_carrera);

   $('.notificacion-overlay').toggleClass('notificacion-activa');
}

function aceptarCarrera()
{
   var id_carrera   =  storage.getItem('id_carrera');
   mostrarPagina('carrera');
   socket.emit( 'carrera_aceptada', id_carrera );
   clearTimeout(timeout);
   clearTimeout(aviso);
}

function rechazarCarrera()
{
   var id_carrera    =  storage.getItem('id_carrera');
   var numero_taxi   =  storage.getItem('numero_taxi');
   var url           =  api+'carrera/rechazar/'+id_carrera;

   $.post( url, function(){})

   .done(function(){
      $('.notificacion-overlay').toggleClass('notificacion-activa');
      var data = { id_carrera : id_carrera, numero_taxi : numero_taxi };
      socket.emit('carrera_rechazada', data);
      calcularPosicion();
      clearTimeout(timeout);
      clearTimeout(aviso);
   })
   .fail(function(){
      alert('no se pudo cancelar la carrera');
   });
}

function buscarCarrera()
{
   var mi_id   =  storage.getItem('id_taxi');
   var url     =  api+'carrera/buscar/'+mi_id;

   $.post(url, function(data){
   })
   .done(function(data){
      var info = data.info;
      if ( ! info )
      {

      } else {

         var nombre                 =  data.carrera.nombre_cliente;
         var apellido               =  data.carrera.apellido_cliente;
         var telefono               =  data.carrera.numero_cliente;
         var direccion              =  data.carrera.direccion;
         var direccion_alternativa  =  data.carrera.direccion_alternativa;
         var id_carrera             =  data.carrera.id_carrera;
         var observacion            =  data.carrera.observacion;

         if ( data.carrera.id_cliente !== null )
         {
            storage.setItem( 'nombre_cliente', String(nombre) );
            storage.setItem( 'apellido_cliente', String(apellido) );
            storage.setItem( 'telefono_cliente', String(telefono) );
            storage.setItem( 'direccion_cliente', String(direccion) );
            storage.setItem( 'direccion_alternativa', String(direccion_alternativa) );
            storage.setItem( 'id_carrera', String(id_carrera) );
            storage.setItem( 'en_carrera', 'true' );
            storage.setItem( 'observacion', observacion );
            mostrarAviso();
         }
      }
   })
   .fail(function(xhr, status, err ){

      var titulo  =  'Error en búsqueda de carrera';
      var mensaje =  'No se pudo buscar carrera:\n'+err;
      var boton   =  'Aceptar';

      nativeAlert( titulo, mensaje, boton );
   });
}

socket.on('recalcular_posicion', function(){
   calcularPosicion();
});

socket.on('nueva_carrera', function( data ){

   var para_id = data;
   var mi_id   = storage.getItem('id_taxi');


   if ( para_id == mi_id )
   {
      $('.barra-tiempo').css('width', '0%' );
      navigator.vibrate([300, 300, 300]);

      cordova.plugins.notification.local.schedule({
         id: getRandomInt(1,100),
         title: "Nueva Carrera",
         text: "Ha recibido una nueva carrera",
         icon: "ic_dialog_map"
      });

      buscarCarrera();

      // LE DAMOS EL TIEMPO PARA ACEPTAR

      function cuentaAtras()
      {
         var tiempoMaximo     =  40000;
         var recordarTiempo   =  20000;
         var cicloAnimacion   =  0;
         var tamano           =  0;



         timeout = setTimeout( function(){
            rechazarCarrera();
         },tiempoMaximo);

         aviso = setTimeout( function(){
            navigator.vibrate(500);
            cordova.plugins.notification.local.schedule({
               id: getRandomInt(1,100),
               title: "Nueva Carrera",
               text: "Le quedan 20 segundos para aceptar la carrera!",
               icon: "ic_dialog_map"
            });
         },recordarTiempo);

         mostrarBarra = setInterval( function(){

            if ( cicloAnimacion <= tiempoMaximo )
            {
               cicloAnimacion += 17;
               tamano += 0.04;
               $('.barra-tiempo').css('width', tamano+'%' );
            }

         },17);
      }

      cuentaAtras();
   }

   function getRandomInt(min, max) {
      return Math.floor(Math.random() * (max - min)) + min;
   }

});
