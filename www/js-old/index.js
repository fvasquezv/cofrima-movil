var app = {

   initialize: function() {
      document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
   },

   onDeviceReady: function(){
      try{
         cordova.plugins.backgroundMode.setEnabled(true);
      } catch( err ) {
         alert(err);
      }
      this.receivedEvent('deviceready');
      ubicarSeccion();
   },
      receivedEvent: function(id) {
   }
};

$(document).ready(function(){
   // lets load jQuery first, shall we?
   app.initialize();

});
