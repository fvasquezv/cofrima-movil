var Route   =  {

   endRun         :  function()  {

      App.map.remove();
      var route      =  Dash.getRouteInfo();
      var id_carrera =  route.route_id;
      var url        =  App.api+'carrera/finalizar/'+id_carrera;

      $.post(url, function(data) {

      }).done(function(data)  {
         App.storage.setItem( 'on_route', '0' );
         Router.getView( 'dash' );
         App.socket.emit('carrera_finalizada');
      })
      .fail(function( xhr, status, error ){

         if ( error == 'Not Found') {
            App.storage.setItem( 'on_route', '0' );
            Router.getView( 'dash' );
            App.loader.fadeOut('fast');
            alert('La carrera ya fué cerrada anteriormente');
         } else {
            alert('No se pudo cerrar la carrera: '+error);
         }
      });

   },

   loadClientData :  function()  {

      var routeInfo  =  Dash.getRouteInfo();

      $('div.nombre').html( routeInfo.client_name+' '+routeInfo.client_lastname );
      $('div.telefono').html( '<a href="tel:'+routeInfo.client_phone+'">'+routeInfo.client_phone+'</a>' );
      $('div.direccion').html( routeInfo.client_address );
      $('div.observacion').html( routeInfo.route_observation );

   },

   verifyRun      :  function()  {

      var routeInfo  =  Dash.getRouteInfo();
      var driver     =  Driver.getInfo();
      var onRoute    =  App.storage.getItem('on_route');
      var my_id      =  driver.driver_id;
      // está en carrera?
      if ( onRoute == '1') {

         var url     =  App.api+'carrera/buscar/'+my_id;
         var me      =  this;

         $.get( url, function(data) {

         }).done(function(data){

            if ( data.info.hay_carrera ) {

               if ( data.carrera.id_cliente != '666' )   {

                  var route = data.carrera;
                  me.storeRoute(route);
                  Router.getView('carrera');

               } else {
                  App.storage.setItem('busy','1');
                  Router.getView('ocupado');
               }

            } else {
               App.storage.setItem( 'on_route', '0' );
               Router.getView( 'dash' );
            }

            App.loader.fadeOut('fast');

         }).fail(function( xhr, status, error ){

            App.loader.fadeOut('fast');
            alert(error);

         });

      } else {
         Router.getView( 'dash' );
      }

   }

};
