/**
* TODO:
* - quitar los loadres, despúes veremos como agregarlos solo a los eventos de botón
* - Agregar los POST de aceptar y rechazar
**/

var Dash =  {

   timer :  30,

   setData           :  function() {

      var onRoute          =  App.storage.getItem('on_route');

      if ( ! onRoute || onRoute == '0' )  {

         var driver           =  Driver.getInfo();
         var nameLabel        =  $('.nombreConductor');
         var cabNumberLabel   =  $('.numeroMovil');

         nameLabel.html( driver.driver_name+' '+driver.driver_lastname );
         cabNumberLabel.html( driver.cab_number+' - <span style="text-transform:uppercase;">['+driver.cab_plate+']</span>' );
         this.calculateQueue();

      } else {

         Router.getView('carrera');

      }


   },

   storeRoute        :  function( routeInfo )  {

      App.storage.setItem( 'route_id', String( routeInfo.id_carrera ) );
      App.storage.setItem( 'route_observation', String( routeInfo.observacion ) );
      App.storage.setItem( 'route_date', String( routeInfo.c_date ) );
      App.storage.setItem( 'client_name', String( routeInfo.nombre_cliente ) );
      App.storage.setItem( 'client_lastname', String( routeInfo.apellido_cliente ) );
      App.storage.setItem( 'client_phone', String( routeInfo.numero_cliente ) );
      App.storage.setItem( 'client_address', String( routeInfo.direccion_alternativa ) );

   },

   removeRoute       :  function()  {

      App.storage.removeItem( 'route_id' );
      App.storage.removeItem( 'route_observation' );
      App.storage.removeItem( 'route_date' );
      App.storage.removeItem( 'client_name' );
      App.storage.removeItem( 'client_lastname' );
      App.storage.removeItem( 'client_phone' );
      App.storage.removeItem( 'client_address' );
      App.storage.removeItem( 'on_route');

   },

   getRouteInfo      :  function()  {

      var routeInfo  =  {
         route_id          :  App.storage.getItem( 'route_id' ),
         route_observation :  App.storage.getItem( 'route_observation' ),
         route_date        :  App.storage.getItem( 'route_date' ),
         client_name       :  App.storage.getItem( 'client_name' ),
         client_lastname   :  App.storage.getItem( 'client_lastname' ),
         client_phone      :  App.storage.getItem( 'client_phone' ),
         client_address    :  App.storage.getItem( 'client_address' )
      };

      return routeInfo;

   },

   showAlert         :  function()  {

      routeInfo   =  this.getRouteInfo();
      $('.alert-data.direccion').html( routeInfo.client_address );
      $('.alert-data.telefono').html( routeInfo.client_phone );
      $('.notificacion-overlay').toggleClass('notificacion-activa');

   },

   searchRoute       :  function()  {

      /**
      * Search for missed calls
      * to clients and routes
      * also, checks the position in queue
      **/

      var driver  =  Driver.getInfo();
      var my_id   =  driver.cab_id;
      var url     =  App.api+'carrera/buscar/'+my_id;
      var me      =  this;

      $.get( url, function(data) {

      }).done(function(data){

         if ( data.info.hay_carrera ) {

            if ( data.carrera.id_cliente != '666' )   {
               App.newRouteAlert();
               var route = data.carrera;
               me.storeRoute(route);
               me.showAlert();
            } else {
               App.storage.setItem('busy','1');
               Router.getView('ocupado');
            }


            if ( App.storage.on_route != '1')  {
               App.startTimer();
            }
         }

         App.loader.fadeOut('fast');

      }).fail(function( xhr, status, error ){

         App.loader.fadeOut('fast');
         alert(error);

      });
   },

   acceptRoute          :  function(id) {

      var routeInfo  =  this.getRouteInfo();
      var routeId    =  routeInfo.route_id;
      App.storage.setItem('on_route', '1');
      Router.getView('carrera');
      App.socket.emit( 'carrera_aceptada', routeId );
      App.loader.fadeOut('fast');
      $('.notificacion-overlay').toggleClass('notificacion-activa');
      App.socket.emit('log', 'El móvil '+App.storage.getItem('numero_taxi')+' ha aceptado la carrera');
   },

   rejectRoute          :  function() {

      /**
      * Reject route
      * changes the route with the ID
      * to 'Rejected' or 3
      **/
      App.loader.fadeOut('fast');
      $('.notificacion-overlay').toggleClass('notificacion-activa');

      var routeInfo     =  this.getRouteInfo();
      var driver        =  Driver.getInfo();
      var routeId       =  routeInfo.route_id;
      var numero_taxi   =  driver.cab_number;
      var url           =  App.api+'carrera/rechazar/'+routeId;
      var me            =  this;

      $.get( url, function(){

      }).done(function(){

         var data = { id_carrera : routeId, numero_taxi : numero_taxi };
         App.socket.emit('carrera_rechazada', data);
         me.removeRoute();

      }).fail(function(xhr, status, error){
         alert('no se pudo cancelar la carrera: '+error);
      });

   },

   calculateQueue       :  function() {

      var url  =  App.api+'fichero/1/fichas';
      $('.posicionFichero').html('Buscando posición en fichero...');

      $.get( url, function(data){

      }).done(function(data){

         var free_cabs_object =  data.taxis.disponibles;
         var queue            =  0;
         var driver           =  Driver.getInfo();
         var search_position  =  true;

         while ( search_position ) {

            if ( ! free_cabs_object ) {

               $('.posicionFichero').html('Todos los móviles ocupados.');
               search_position   =  false;
               break;

            } else {

               var total_free_cabs  =  data.taxis.disponibles.length;
               var cab_id  =  free_cabs_object[queue].id_taxi;
               queue++;

               if ( cab_id == driver.cab_id )
               {
                  $('.posicionFichero').html(queue);
                  break;
               }

               if ( queue == total_free_cabs ) {
                  search_position = false;
                  $('.posicionFichero').html('Móvil ocupado. Solicite libre a central.');
                  break;
               }
            }
         }

         App.loader.fadeOut('fast');

      }).fail(function(xhr, status, err){
         App.loader.fadeOut('fast');
         alert(err);
      });
   }

};
