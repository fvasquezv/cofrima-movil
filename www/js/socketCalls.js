/**
* TODO:
* - revisar si el usuario está logueado antes de activar
*   funciones por socket
* - Verificar si corresponde ejecutar el método según el view en el que estemos
*   por ejemplo, si estamos en carrera, para que vamos a recalcular o si estamos
*   en carrera para que vamos a recibir otra.
**/

App.socket.on('recalcular_posicion', function() {

   var user =  Driver.getInfo();
   if ( user.driver_id )   {
      Dash.calculateQueue();
   }

});

App.socket.on('nueva_carrera', function( data ) {

   var user          =  Driver.getInfo();
   var driver_id_for =  data;

   if ( user.cab_id ==  driver_id_for ) {
      Dash.searchRoute();
   }

});
