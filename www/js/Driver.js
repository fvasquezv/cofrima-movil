var Driver  =  {


   getInfo           :  function() {

      /**
      * returns an object containing
      * all user info in app storage.
      * return object
      **/

      var driver_info      =  {

         driver_id         :  App.storage.getItem('id_usuario'),
         driver_name       :  App.storage.getItem('nombre'),
         driver_lastname   :  App.storage.getItem('apellido'),
         driver_route      :  App.storage.getItem('en_carrera'),
         cab_id            :  App.storage.getItem('id_taxi'),
         cab_number        :  App.storage.getItem('numero_taxi'),
         cab_plate         :  App.storage.getItem('patente_taxi')

      };

      return driver_info;
   },

   saveDriverData    :  function( id, nombre, apellido ) {

      /**
      * Saves local variables to
      * the local storage.
      **/

      App.storage.setItem( 'id_usuario', String(id) );
      App.storage.setItem( 'nombre', String(nombre) );
      App.storage.setItem( 'apellido', String(apellido) );

   },

   saveCabData       :  function( id, numero, patente ) {

      /**
      * Saves local variables to
      * the local storage.
      **/

      App.storage.setItem( 'id_taxi', String( id ) );
      App.storage.setItem( 'numero_taxi', String( numero ) );
      App.storage.setItem( 'patente_taxi', String( patente ) );

   },

   logOut            :  function() {

      /**
      * Remove al items from the
      * local storage
      **/

      var idTaxi    =  App.storage.getItem( 'id_taxi' );
      var postData   =  { id_taxi : idTaxi, id_cliente : 9000 };
      var url        =  App.api+'carrera/nueva';

      $.post( url, postData, function(data){

      }).done(function(){
         App.socket.emit('log', 'El móvil '+App.storage.getItem('numero_taxi')+' ha cerrado sesión');
      }).fail(function(xhr, status, error){
         alert(error);
      });

      App.storage.removeItem( 'id_usuario'   );
      App.storage.removeItem( 'nombre' );
      App.storage.removeItem( 'apellido'  );
      App.storage.removeItem( 'id_taxi'   );
      App.storage.removeItem( 'numero_taxi'  );
      App.storage.removeItem( 'patente_taxi' );

      Router.getView('login');
      App.loader.fadeOut('fast');

   },

   doLogin           :  function() {

      /**
      * Request Login
      * get login verification with Ajax post
      * to rest server.
      **/

      var usuario    =  $('input[name="usuario"]').val();
      var password   =  $('input[name="password"]').val();

      if ( ! usuario || ! password ) {

         App.loader.fadeOut('fast');
         var mensaje =  'Debe rellenar ambos campos!';
         alert( mensaje );

      } else {

         var url  =  App.api+'usuario/verificar';
         var me   =  this;

         var data =  {
            usuario  : usuario,
            password : password
         };

         $.post( url, data, function(res){

         }).done( function(res){

            if ( res.info.estado ) {
               me.saveDriverData( res.usuario.id_conductor, res.usuario.nombre, res.usuario.apellido );
               if ( res.taxi ) {
                  me.saveCabData( res.taxi.id_taxi, res.taxi.numero, res.taxi.patente );
                  Router.getView('dash');
               } else {
                  Router.getView('taxi');
               }
               App.loader.fadeOut('fast');
            } else {
               App.loader.fadeOut('fast');
               alert(res.info.message);
            }

         }).fail(function( xhr, status, err ){
            App.loader.fadeOut('fast');
            alert(err);
         });
      }
   }

};
