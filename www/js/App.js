var App  =  {

   time           :  40, // in seconds

   timeout        :  '',

   reminder       :  '',

   progressBar    :  '',

   map            :  '',

   initialize     :  function() {
      document.addEventListener('deviceready', this.onDeviceReady.bind(this), false );
   },

   onDeviceReady  :  function(){

      try{
         cordova.plugins.backgroundMode.setEnabled(true);
      } catch( err ) {
         alert(err);
      }

      this.receivedEvent('deviceready');

   },

   receivedEvent  :  function(id) {

   },

   canvas         :  $('div.app'),

   storage        :  window.localStorage,

   loader         :  $('div.loading'),

   api            :  "http://cofrima.bdoce.cl/api/",

   socket         :  io.connect('http://cofrima.bdoce.cl:9000'),

   newRouteAlert  :  function()  {

      var me   =  this;

      if ( App.storage.on_route != '1')  {
         cordova.plugins.notification.local.schedule({
            id: me.getRandomInt(1,100),
            title: "Nueva Carrera",
            text: "Ha recibido una nueva carrera",
            icon: "ic_dialog_map"
         });
      }


   },

   reminderAlert  :  function()  {

      var me   =  this;

      if ( App.storage.on_route != '1')  {
         cordova.plugins.notification.local.schedule({
            id: me.getRandomInt(1,100),
            title: "Carrera Pendiente",
            text: "Le quedan 20 segundos para aceptar la carrera!",
            icon: "ic_dialog_map"
         });
      }
   },

   getRandomInt   :  function(min, max)   {
      return Math.floor(Math.random() * (max - min)) + min;
   },

   startTimer     :  function()  {

      var me   =  this;
      me.startProgressBar();
      me.timeout  =   setTimeout( function()  {

         // se cancela la carrera
         App.loader.fadeIn('fast');
         Dash.rejectRoute();

      }, me.time * 1000 );

      me.reminder =  setTimeout( function()  {
         me.reminderAlert();
      }, ( me.time * 1000 ) / 2 );

   },

   stopTimer      :  function()  {
      var me   =  this;
      clearTimeout(  me.timeout  );
      clearTimeout(  me.reminder );
      me.stopProgressBar();
   },

   startProgressBar  :   function()  {

      $('.barra-tiempo').css('width', '0%');

      var tamano           =  0;
      var tiempoMaximo     =  40000;
      var cicloAnimacion   =  0;

      this.progressBar = setInterval( function(){

         if ( cicloAnimacion <= tiempoMaximo )
         {
            cicloAnimacion += 17;
            tamano += 0.04;
            $('.barra-tiempo').css('width', tamano+'%' );
         }

      }, 17);
   },

   stopProgressBar   :  function()  {
      var me   =  this;
      clearInterval( me.progressBar );
      $('.barra-tiempo').css('width', '0%');
   }

};
