   var app_container =  $('div.app');
   var spinner       =  $('div.loading');
   var api           =  'http://cofrima.bdoce.cl/api/';
   var storage       =  window.localStorage;
   var socket        =  io.connect('http://rtc.fabianvasquez.com:9000');

   function ubicarSeccion()
   {
      var hay_usuario   =  ( storage.getItem('id_usuario') !== null ) ? true : false;
      var hay_taxi      =  ( storage.getItem('id_taxi') !== null && storage.getItem('id_taxi') !== '0'   ) ? true:false;

      if ( hay_usuario && hay_taxi )
      {
         mostrarPagina('dash');
      }

      if ( hay_usuario  &&  ! hay_taxi )
      {
         mostrarPagina('taxi');
      }

      if ( ! hay_usuario )
      {
         mostrarPagina('login');
      }

   }

   function mostrarPagina( pagina )
   {

      spinner.fadeIn('fast');
      app_container.html('');

      $.get('pages/'+pagina+'.html', function(data){})

      .done(function(data){
         app_container.html(data);
         spinner.fadeOut('slow');
      })

      .fail(function( xhr, status, err ){
         spinner.fadeOut('fast');
         nativeAlert( 'Error de la aplicación', 'No se pudo cargar la vista. Contacte soporte.', 'Aceptar' );
      });
   }


   /**
   *  GLOBALS
   **/

   function alertDismissed() {
      // nada
   }

   function cerrarSesion()
   {
      storage.clear();
   }

   function nativeAlert( titulo, mensaje, boton )
   {
      navigator.notification.alert(
         mensaje,
         alertDismissed,
         titulo,
         boton
      );
   }
