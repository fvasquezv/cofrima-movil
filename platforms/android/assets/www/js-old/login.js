$(document).on('click', '.btnLogin', function(){

   spinner.fadeIn('fast');

   var usuario    =  $('input[name="usuario"]').val();
   var password   =  $('input[name="password"]').val();
   var titulo     =  'Error en inicio de sesión';
   var boton      =  'Aceptar';

   if ( !usuario || !password )
   {
      spinner.fadeOut('fast');
      var mensaje =  'Debe rellenar ambos campos!';
      nativeAlert( titulo, mensaje, boton );

   } else {

      var url  =  api+'usuario/verificar';
      var data =  { usuario: usuario, password: password };


      $.post( url, data, function(res){})

      .done( function(res){

         spinner.fadeOut('fast');
         var estado  =  res.info.estado;

         if ( ! estado )
         {
            spinner.fadeOut('fast');
            var mensaje =  res.info.message;
            nativeAlert( titulo, mensaje, boton );

         } else {

            var usuario =  res.usuario;
            var taxi    =  res.taxi;
            spinner.fadeOut('fast');

            storage.setItem( 'id_usuario', String(usuario.id_conductor) );
            storage.setItem( 'nombre', String(usuario.nombre) );
            storage.setItem( 'apellido', String(usuario.apellido) );

            socket.emit('log', 'el usuario '+usuario.nombre+' '+usuario.apellido+' ha iniciadio sesión');

            if ( ! taxi ) {

               mostrarPagina('taxi');

            } else {

               storage.setItem( 'id_taxi', String( taxi.id_taxi ) );
               storage.setItem( 'numero_taxi', String( taxi.numero ) );
               storage.setItem( 'patente_taxi', String( taxi.patente ) );
               mostrarPagina('dash');

            }

         }

      })

      .fail(function( xhr, status, err ){

         var titulo  =  'Error en inicio de sesión';
         var mensaje =  'No se pudo iniciar sesión:\n'+err;
         var boton   =  'Aceptar';

         nativeAlert( titulo, mensaje, boton );

      });

   }
});
