$(document).on('click', '#buscar_asociado', function(){
   asignarTaxi();
});

function asignarTaxi()
{

   $('.buscandoMovil').html('Buscando móvil...');
   // buscar
   var id_usuario =  storage.getItem('id_usuario');
   var url        =  api+'usuario/'+id_usuario;

   $.get(url, function(data){ })

   .done( function(data){

      var taxi =  data.taxi;

      if ( ! taxi )
      {
         socket.emit('log', 'el usuario '+storage.getItem('nombre')+' '+storage.getItem('apellido')+' no tiene un móvil asociado');
         var mensaje =  'No se han encontrado taxis asociados a su usuario. Comuníquese con central para solicitar uno';
         $('.buscandoMovil').html( mensaje );
         navigator.vibrate([200, 500, 200 ]);

      } else {
         storage.setItem( 'id_taxi', taxi.id_taxi );
         storage.setItem('numero_taxi', taxi.numero );
         storage.setItem( 'patente_taxi', String( taxi.patente ) );
         mostrarPagina('dash');

      }
   })
   .fail( function( xhr, status, err ){
      alert(err);
      alert(url);
   });
}
