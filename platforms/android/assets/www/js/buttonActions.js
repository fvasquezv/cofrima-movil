/*****************************************************/
/*          LOGIN VIEW BUTTONS                       */
/*****************************************************/

$(document).on( 'click', '.btnLogin', function() {
   App.loader.fadeIn('fast');
   Driver.doLogin();
});


/*****************************************************/
/*          DASH VIEW BUTTONS                        */
/*****************************************************/

$(document).on( 'click', '.logOut', function() {
   App.loader.fadeIn('fast');
   Driver.logOut();
});

$(document).on( 'click', '.buscarCarrera', function() {
   App.loader.fadeIn('fast');
   Dash.searchRoute();
   Dash.calculateQueue();
});

$(document).on( 'click', '.crearCarrera', function() {
   Busy.start();
});

$(document).on( 'click', '.btn-aceptar', function() {
   App.loader.fadeIn('fast');
   App.stopTimer();
   Dash.acceptRoute();
});

$(document).on( 'click', '.btn-cancelar', function() {
   App.loader.fadeIn('fast');
   App.stopTimer();
   Dash.rejectRoute();
});

/********************************************************/
/*          Carrera View                                */
/********************************************************/

$(document).on( 'click', '.cerrarCarrera', function() {
   App.loader.fadeIn('fast');
   Route.endRun();
});

/********************************************************/
/*          Busy View                                   */
/********************************************************/

$(document).on( 'click', '.libre', function()   {
   Busy.end();
});
