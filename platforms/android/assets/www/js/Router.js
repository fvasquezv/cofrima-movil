var Router  =  {

   getView     :  function(view){

      /**
      * Load a view's html markup
      * via AJAX and add it to the
      * App.canvas
      **/

      App.loader.fadeIn('fast');

      var url  =  'pages/'+view+'.html';

      $.get( url, function(data){

      }).done(function(data){

         App.canvas.html(data);
         App.loader.fadeOut('fast');

      }).fail(function(xhr, status, err){

         App.loader.fadeOut('fast');
         alert(err);

      });

   },

   findView    :  function() {

      /**
      * Returns a view based on the
      * Driver user's data
      **/

      var section;
      var driver  =  Driver.getInfo();

      if ( ! driver.driver_id ) {
         section  =  'login';
      }

      if ( driver.driver_id && ! driver.cab_id ) {
         section  =  'taxi';
      }

      if ( driver.driver_id && driver.cab_id ) {
         section  =  'dash';
      }

      if ( App.storage.getItem('busy') == '1' ) {
         // verificar que aun existe la carrera
         Route.verifyRun();
      }

      if ( driver.driver_id && driver.cab_id && driver.driver_route ) {
         Route.verifyRun();
      }



      Router.getView(section);
   }

};
