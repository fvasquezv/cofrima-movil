$(document).ready(function(){

   App.initialize();
   Router.findView();

   var driver  =  Driver.getInfo();

   setInterval(function(){

      if ( driver.cab_number )
      {
         getGeolocation();
      }

   }, 7000 );

   function getGeolocation() {
      navigator.geolocation.getCurrentPosition( geolocationSuccess, geolocationSuccess, { maximuAge: 3000, timeout: 5000, enableHighAccuracy: true } );
   }

   var geolocationSuccess = function(position) {

      var data = {
         latitud     :  position.coords.latitude,
         longitud    :  position.coords.longitude,
         direccion   :  position.coords.heading,
         velocidad   :  position.coords.speed,
         numero_taxi :  driver.cab_number
      };

      App.socket.emit( 'enviar_ubicacion', data );

   }

   function geolocationError(error) {

      alert('code: '    + error.code + '\n'+
            'mensaje: ' +error.message+'\n'
      );
   }

});
