var mapCounter = 0;

function initMap() {

   var routeInfo  =  Dash.getRouteInfo();
   var direccion  =  routeInfo.client_address+', Punta Arenas, Chile';
   var div        =  document.getElementById('map');
   var address    =  direccion.replace(/^[0]+/g,"");
   App.map        =  plugin.google.maps.Map.getMap(div);

   plugin.google.maps.Geocoder.geocode( { 'address' : address }, function(results){

      if ( results.length ) {

         App.storage.setItem('hay_mapa', 'true');
         var result     =  results[0];
         var position   =  result.position;
         App.map.setBackgroundColor('#4e4f51');
         App.map.addMarker({
            'position'  :  position,
            'title'     :  JSON.stringify( result.position )
         }, function( marker ){

            App.map.moveCamera({
               'target' :  position,
               'zoom'   :  16
            }, function(){
               marker.shoeInfoWindow();
            });
         });
      }

   });

}
