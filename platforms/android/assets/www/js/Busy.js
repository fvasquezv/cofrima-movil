var Busy =  {

   start: function() {

      var driver  =  Driver.getInfo();
      var url     =  App.api+'crear-carrera';
      var data    =  { id_taxi : driver.cab_id };
      var me      =  this;
      $.post( url, data, function(){})

      .done(function(){

         App.storage.setItem('on_route', '1');
         App.storage.setItem('busy', '1');
         Router.getView('ocupado');
         App.loader.fadeOut('fast');
         me.addToRoute();
         App.socket.emit('carrera_finalizada');

      })
      .fail(function( xhr, status, err ){
         App.loader.fadeOut('fast');
         Router.findView();
         alert(err);
      });

   },

   end:  function()  {

      App.storage.setItem('busy', '0');
      Route.endRun();
      App.loader.fadeOut('fast');

   },

   addToRoute: function()  {

      var driver  =  Driver.getInfo();
      var my_id   =  driver.cab_id;
      var url     =  App.api+'carrera/buscar/'+my_id;

      $.get( url, function(data) {

      }).done(function(data){

         if ( data.info.hay_carrera ) {
            var route = data.carrera;
            Dash.storeRoute(route);
         }

      }).fail(function( xhr, status, error ){

         alert(error);

      });
   }

};
